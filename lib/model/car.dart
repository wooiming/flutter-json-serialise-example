import 'package:json_annotation/json_annotation.dart';
import 'image.dart';

part 'car.g.dart';

@JsonSerializable()
class Car {

  String id;
  int leadId;
  int inspectionId;

  @JsonKey(name: 'carFullname') // map the response json key to this property
  String carFullName;

  int carYear;
  String carBrand;
  String carModel;
  double carEngine;
  String carVariant;
  String carTransmission;
  String currency;
  int startPrice;
  int mileage;
  String mileageUnit;

  @JsonKey(name: 'imageGallery')
  List<Image> images;
  
  String get coverImageThumbnail => images.first.thumbnailUrl;

  String get priceDisplay => "$currency $startPrice";

  String get mileageDisplay => "$mileage $mileageUnit";

  Car();

  factory Car.fromJson(Map<String, dynamic> json) => _$CarFromJson(json);
  Map<String, dynamic> toJson() => _$CarToJson(this);
}