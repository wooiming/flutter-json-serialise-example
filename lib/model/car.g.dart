// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'car.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Car _$CarFromJson(Map<String, dynamic> json) {
  return Car()
    ..id = json['id'] as String
    ..leadId = json['leadId'] as int
    ..inspectionId = json['inspectionId'] as int
    ..carFullName = json['carFullname'] as String
    ..carYear = json['carYear'] as int
    ..carBrand = json['carBrand'] as String
    ..carModel = json['carModel'] as String
    ..carEngine = (json['carEngine'] as num)?.toDouble()
    ..carVariant = json['carVariant'] as String
    ..carTransmission = json['carTransmission'] as String
    ..currency = json['currency'] as String
    ..startPrice = json['startPrice'] as int
    ..mileage = json['mileage'] as int
    ..mileageUnit = json['mileageUnit'] as String
    ..images = (json['imageGallery'] as List)
        ?.map(
            (e) => e == null ? null : Image.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$CarToJson(Car instance) => <String, dynamic>{
      'id': instance.id,
      'leadId': instance.leadId,
      'inspectionId': instance.inspectionId,
      'carFullname': instance.carFullName,
      'carYear': instance.carYear,
      'carBrand': instance.carBrand,
      'carModel': instance.carModel,
      'carEngine': instance.carEngine,
      'carVariant': instance.carVariant,
      'carTransmission': instance.carTransmission,
      'currency': instance.currency,
      'startPrice': instance.startPrice,
      'mileage': instance.mileage,
      'mileageUnit': instance.mileageUnit,
      'imageGallery': instance.images,
    };
