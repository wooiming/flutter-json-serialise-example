import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:json_serialize_example/model/car.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'JSON Serialize Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Demo Homepage'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Car> cars = [];
  Dio dio;

  _MyHomePageState() {
    dio = Dio();
  }

  _getList() async {
    Response response = await dio.get("http://13.229.73.199/bidding/jit/car");
    List<dynamic> resultData = response.data['data'];

    setState(() {
      cars.addAll(resultData.map( (data) => Car.fromJson(data) ));  
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: cars.length,
          itemBuilder: (context, index) {
            Car car = cars[index];
            return ListTile(
              leading: _fetchImage(car.coverImageThumbnail),
              title: Text(car.carFullName),
              subtitle: Row(
                children: <Widget>[
                  Text(car.priceDisplay),
                  Spacer(),
                  Text(car.mileageDisplay),
                  Spacer(),
                ]
              ),
            );
          },
        )
      ),
      floatingActionButton: FloatingActionButton(
      onPressed: _getList,
        tooltip: 'Get list',
        child: Icon(Icons.add),
      ),
    );
  }

  _fetchImage(url) {
    return CachedNetworkImage(
      imageUrl: url,
      placeholder: (context, url) => CircularProgressIndicator(),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}
